import axios, { AxiosError, AxiosRequestConfig } from "axios"
import { LoginService } from "../services/login.service"


export const authInterceptor = (loginService:LoginService): void => {
    // Add a request interceptor
    axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        if(config.headers)config.headers['Authorization'] = loginService.getCurrentUserBasicAuthentication()
        return config
      },
      (error: AxiosError) => {
        console.error('ERROR:', error)
        Promise.reject(error)
      }
    )
  }