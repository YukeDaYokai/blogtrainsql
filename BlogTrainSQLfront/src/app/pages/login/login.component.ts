import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

public username: string="";

public password: string="";

constructor(private route:Router, private loginService: LoginService) { }

// VERSION TROP COOL ADRIEN, MAIS PAS REUSSI A ADAPTER AVEC LES DONNES D'AMANDINE
// Création d'une méthode pour récupérer les entrées utilisateurs et les enregistrer en local
  // onSubmit(f: NgForm) {
  //   console.log(f.value);  // { login: '', password: '' }
  //   localStorage.setItem("User", f.value.login)
  //   console.log(f.valid);  // false
  //   localStorage.setItem("Password",f.value.password)
  //   this.route.navigate(["/"])
  // }

  onSubmitLoggin(){
    this.loginService.login({username: this.username, password: this.password});
    this.loginService.verify()
    .then(()=>this.route.navigate(["/"]))
    .catch(()=>this.loginService.logout())
  }

// Private route:Router permet de revenir à la page principale après avoir réalisé l'action (ici se log).
// Attention : il faut le préciser dans la méthode au dessus (onSubmit).

  ngOnInit(): void {
  }

}
