import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss']
})
export class CreateArticleComponent implements OnInit {

  title:string = '';
  summary:string= '';
  content:string= '';

  constructor(public articleService: ArticleService, private route:Router) { }

  ngOnInit(): void {
  }

// Private route:Router permet de revenir à la page principale après avoir réalisé l'action (ici la création d'un article).
// Attention : il faut le préciser dans la méthode suivante (createArticle).
  createArticle(){
    this.articleService.createArticle(this.title, this.summary, this.content);
    this.route.navigate(["/"])
}

}
