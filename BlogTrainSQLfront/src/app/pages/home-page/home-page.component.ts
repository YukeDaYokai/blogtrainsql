import { Component, Input, OnInit, Output } from '@angular/core';
import { Article } from 'src/app/model/articles.model';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  // On crée un tableau d'articles pour y stocker nos articles
  public articles:Article[] = [];

  constructor(public articleService: ArticleService) { }

  // On appelle les méthodes qui nous intéressent à partir de articleService
  async ngOnInit() {
    this.articles = await this.articleService.fetch()
  }

  // La méthode suivante permet de refresh après une suppression
  async reloadArticles(id:string){
    console.log("Reload because of delete:",id)
    this.articles = await this.articleService.fetch()
  }

}
