import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Article } from 'src/app/model/articles.model';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  @Input()
  public article?: Article;

  @Output()
  public deleteEvent = new EventEmitter<string>();


  constructor(public articleService: ArticleService) { }

  async ngOnInit() {
  }

  deleteArticle(id: string){
    this.articleService.deleteArticle(id);
    this.deleteEvent.emit(id);
  }

}
