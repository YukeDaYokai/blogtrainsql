import { Component, OnInit } from '@angular/core';
import { authInterceptor } from './Helpers/interceptor';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'BlogTrainSQLfront';

  constructor(private loginService: LoginService){
    authInterceptor(this.loginService)
  }
}

