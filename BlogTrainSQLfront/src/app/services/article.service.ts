import { Injectable } from '@angular/core';
import axios from 'axios';
import { Article } from '../model/articles.model';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private apiUrl: string = 'http://localhost:8080/api/articles';
  
  public async fetch(): Promise <Article[]>{
    const {data}=await axios.get(`${this.apiUrl}`);
    return data.map((article: any)=> {
      return{
      id: article.id,
      title: article.title,
      summary: article.summary,
      content: article.content,
      }
    });
  }

  // public async loadArticle(){
  //    await this.fetchArticles();
  // }

  // // On crée une méthode pour afficher une liste de tous nos articles
  // public fetchArticles(){
  //   axios.get('http://localhost:8080/api/articles')
  //   .then((res)=>{
  //     console.log(res.data);
  //   })
  //   .catch((err)=>{
  //     console.log(err);
  //   });
  // }

  // On crée une méthode pour créer un article en dur, envoyé dans notre BDD
  public createArticle(title:string, summary:string, content:string){
    const apiUrl = 'http://localhost:8080/api/articles';
    axios.post(`${apiUrl}`,{title, summary, content}, {headers:{'Authorization': 'Basic dXNlcjpsYW1h'}})
    .then((res)=>{
      console.log(res.data);
    })
    .catch((err)=>{
      console.log(err);
    });
  }

  // On crée une méthode pour effacer un article par son id
  public deleteArticle(id: string){
    const apiUrl='http://localhost:8080/api/articles';
    axios.delete(`${apiUrl}/${id}`)
    .then(res => console.log(res) )
    .catch((err)=>{
      console.log(err);
    });
  }

  constructor() { }
}
