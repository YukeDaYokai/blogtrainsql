package com.example.BlogTrainSQL.application;

import com.example.BlogTrainSQL.domain.model.article.Article;
import com.example.BlogTrainSQL.domain.model.repository.ArticleRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ArticleService {
    private final ArticleRepository articleRepository;

    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Article createArticle(String title, String summary, String content) {
        Article newArticle = new Article(UUID.randomUUID().toString(), title, summary, content);
        // .save est une méthode implémentée par CrudRepository
        articleRepository.save(newArticle);
        return newArticle;
    }

    public Optional<Article> findById(String articleId) {
        return articleRepository.findById(articleId);
    }

    public void deleteArticle(String articleId) {
        articleRepository.deleteById(articleId);
    }

    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    public Article updateArticle (String articleId, String title, String summary, String content) {
        Article articleToUpdate=articleRepository.findById(articleId).orElseThrow();
        articleToUpdate.setTitle(title);
        articleToUpdate.setSummary(summary);
        articleToUpdate.setContent(content);
        articleRepository.save(articleToUpdate);
        return articleToUpdate;
    }
}
