package com.example.BlogTrainSQL.domain.model.repository;

import com.example.BlogTrainSQL.domain.model.article.Article;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends MongoRepository<Article,String> {
}
