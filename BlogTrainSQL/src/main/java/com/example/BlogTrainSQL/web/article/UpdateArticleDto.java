package com.example.BlogTrainSQL.web.article;

public record UpdateArticleDto(String title, String content, String summary) {
}
