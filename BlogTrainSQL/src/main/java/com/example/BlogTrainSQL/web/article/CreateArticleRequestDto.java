package com.example.BlogTrainSQL.web.article;

public record CreateArticleRequestDto (String title, String summary, String content){

}
