package com.example.BlogTrainSQL.web.article;

import com.example.BlogTrainSQL.application.ArticleService;
import com.example.BlogTrainSQL.domain.model.article.Article;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleService articleService;

    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    // La méthode suivante permet de créer un article)
    @PostMapping
    ResponseEntity<Article> createArticle(@RequestBody CreateArticleRequestDto body) {
        Article createdArticle = articleService.createArticle(body.title(), body.summary(), body.content());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdArticle);
    }

    // La méthode suivante permet de retrouver un article par son ID
    @GetMapping("/{articleId}")
    ResponseEntity<Article> findArticle(@PathVariable String articleId) {
        return articleService.findById(articleId).map(u -> ResponseEntity.ok(u)).orElseGet(() -> ResponseEntity.notFound().build());
    }


    // La méthode suivante permet de lister tous les articles
    @GetMapping()
    ResponseEntity<List<Article>> findAllArticle() {
        return  ResponseEntity.ok(articleService.findAll());
    }


    // A noter qu'on utilisera le verbe http PUT plutôt que PATCH, parce que PUT remplace toute la donnée. Le PATCH
    // ne fera qu'une modification partielle.
    @PutMapping()
    ResponseEntity<Article> editArticle (@PathVariable String articleId, @RequestBody UpdateArticleDto body){
        Article editedArticle = articleService.updateArticle(articleId, body.title(), body.summary(), body.content());
        return ResponseEntity.status(HttpStatus.OK).body(editedArticle);
    }


    // La méthode suivante permet d'effacer un article
    @DeleteMapping("/{articleId}")
    @ResponseStatus(HttpStatus.OK)
    void deleteArticle(@PathVariable String articleId) {
        articleService.deleteArticle(articleId);
    }

}
