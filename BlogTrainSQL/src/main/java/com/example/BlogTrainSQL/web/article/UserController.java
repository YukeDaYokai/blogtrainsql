package com.example.BlogTrainSQL.web.article;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping ("/users")
public class UserController {

    @PostMapping("/me")
    @ResponseStatus(value = HttpStatus.OK)
    public String me(){
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
