package com.example.BlogTrainSQL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogTrainSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogTrainSqlApplication.class, args);
	}

}
